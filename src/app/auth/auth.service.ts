import { Injectable } from '@angular/core';
import { User, UserRole } from '../models/User.model';

@Injectable()
export class AuthService {
  constructor() {}

  public isAuthenticated(): boolean {
    return this.getUserFromStorage().role !== UserRole.NONE;
  }

  public getUserFromStorage(): User {
    const emptyUser: User = {
      id: 0,
      name: '',
      email: '',
      password: '',
      role: UserRole.NONE
    };

    const userFromStorage = localStorage.getItem("user");
    let parsedUserFromStorage: User = emptyUser;
    
    if (userFromStorage) {
      parsedUserFromStorage = JSON.parse(userFromStorage); 
    }
     
    return parsedUserFromStorage;
  }

  public setUserInStorage(user: User) {
    localStorage.setItem("user", JSON.stringify(user));
  }

  public removeUserFromStorage() {
    localStorage.removeItem("user")
  }
}