import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faHotel } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  faHotel = faHotel;
  name!: string;

  constructor(private router: Router, public authService: AuthService) {
  }

  ngOnInit(): void {
    this.name = this.authService.getUserFromStorage().name;
  }

  navigateTo(url: string): void {
    this.router.navigate([url]);
  }

  logout() {
    this.authService.removeUserFromStorage();
    this.router.navigate(["login"]);
  }
}
