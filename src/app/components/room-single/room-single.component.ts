import { Component, OnInit } from '@angular/core';
import { Gallery, GalleryItem, ImageItem } from 'ng-gallery';

@Component({
  selector: 'app-room-single',
  templateUrl: './room-single.component.html',
  styleUrls: ['./room-single.component.scss']
})
export class RoomSingleComponent implements OnInit {
  images:GalleryItem[];
  constructor() {
    this.images=[];
   }

  ngOnInit(): void {
    this.images = [
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/Ednospalen-Apartament-Arte-Spa-Park-Hotel-10.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/Ednospalen-Apartament-Arte-Spa-Park-Hotel-10.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/Dvoina-Staq-Arte-Spa-Park-Hotel-Velingrad-11.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/Dvoina-Staq-Arte-Spa-Park-Hotel-Velingrad-11.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-standartna-staq-arte-spa-park-hotel.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-standartna-staq-arte-spa-park-hotel.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/Ednospalen-Apartament-Arte-Spa-Park-Hotel-11.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/Ednospalen-Apartament-Arte-Spa-Park-Hotel-11.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-arte-spa-park-hotel-6.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-arte-spa-park-hotel-6.jpg' }),
      
    ];
  }

}
