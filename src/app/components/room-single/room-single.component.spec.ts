import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomSingleComponent } from './room-single.component';

describe('RoomSingleComponent', () => {
  let component: RoomSingleComponent;
  let fixture: ComponentFixture<RoomSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoomSingleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RoomSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
