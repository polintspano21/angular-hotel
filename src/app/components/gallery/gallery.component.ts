import { Component, OnInit } from '@angular/core';

export interface Tile {
  imageLink: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  tiles: Tile[] = [
    {text: 'One', cols: 2, rows: 1, imageLink: 'https://i.pinimg.com/564x/4c/75/2a/4c752a39017eff34e075f2295dece296.jpg'},
    {text: 'Two', cols: 1, rows: 2, imageLink: 'https://i.pinimg.com/564x/0a/11/8f/0a118f616f521626f006885ecd65528f.jpg'},
    {text: 'Three', cols: 1, rows: 2, imageLink: 'https://i.pinimg.com/564x/60/1e/c2/601ec25e44d9f5914d547f5f9c5e7cca.jpg'},
    {text: 'Four', cols: 2, rows: 1, imageLink:'https://i.pinimg.com/564x/8e/f0/be/8ef0be1a2faa77097e455f74766cdcfb.jpg'},
  ];


  constructor() { }

  ngOnInit(): void {
  }

}
