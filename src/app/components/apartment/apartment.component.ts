import { Component, OnInit } from '@angular/core';
import { Gallery, GalleryItem, ImageItem } from 'ng-gallery';

@Component({
  selector: 'app-apartment',
  templateUrl: './apartment.component.html',
  styleUrls: ['./apartment.component.scss']
})
export class ApartmentComponent implements OnInit {
  images:GalleryItem[];
  constructor() {
    this.images=[];
   }

  ngOnInit(): void {
    this.images = [
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2017/10/dvoina-staq-arte-spa-park-hotel.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2017/10/dvoina-staq-arte-spa-park-hotel.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2017/10/dvoina-staq-arte-spa-park-hotel-banq-.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2017/10/dvoina-staq-arte-spa-park-hotel-banq-.jpg' }),
      new ImageItem({ src: 'https://solvexstatic.peakview.bg/img/OBEKTI/5146/BIG_-4_14931859605146.jpg', thumb: 'https://solvexstatic.peakview.bg/img/OBEKTI/5146/BIG_-4_14931859605146.jpg' }),
      new ImageItem({ src: 'https://anteatravel.com/images/oferti/big/Arte-Spa-Park-hotel-3.jpg', thumb: 'https://anteatravel.com/images/oferti/big/Arte-Spa-Park-hotel-3.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-arte-spa-park-hotel-6.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-arte-spa-park-hotel-6.jpg' }),
      
    ];
  }

}
