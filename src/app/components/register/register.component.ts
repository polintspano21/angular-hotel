import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { User, UserRole } from 'src/app/models/User.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formGroup!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private _http: HttpClient ) { 
  }

  ngOnInit(): void {
    if(this.authService.isAuthenticated()) {
      this.router.navigate(['/bookings']);
    }

    this.formGroup = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  goToLogin(): void {
    this.router.navigate(['/login'])
  }

  register() {
    this._http.post<User>(`${environment.apiUrl}/users`, { ...this.formGroup.value, role: UserRole.USER })
    .subscribe({
      next: () => this.router.navigate(["login"])
    });
 }

}
