import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApartamentVipComponent } from './apartament-vip.component';

describe('ApartamentVipComponent', () => {
  let component: ApartamentVipComponent;
  let fixture: ComponentFixture<ApartamentVipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApartamentVipComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ApartamentVipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
