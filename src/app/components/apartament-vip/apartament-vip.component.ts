import { Component, OnInit } from '@angular/core';
import { Gallery, GalleryItem, ImageItem } from 'ng-gallery';

@Component({
  selector: 'app-apartament-vip',
  templateUrl: './apartament-vip.component.html',
  styleUrls: ['./apartament-vip.component.scss']
})
export class ApartamentVipComponent implements OnInit {
  images:GalleryItem[];
  constructor() {
    this.images=[];
   }

  ngOnInit(): void {
    this.images = [
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2017/10/prezidentski-apartament-arte-spa-park-hotel-sys-sauna.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2017/10/prezidentski-apartament-arte-spa-park-hotel-sys-sauna.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/presidentski-apartament-arte-spa-park-hotel.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/presidentski-apartament-arte-spa-park-hotel.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2017/10/prezidentski-apartament-arte-spa-park-hotel-5.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2017/10/prezidentski-apartament-arte-spa-park-hotel-5.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2017/10/prezidentski-apartament-arte-spa-park-hotel-4.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2017/10/prezidentski-apartament-arte-spa-park-hotel-4.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-arte-spa-park-hotel-6.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-arte-spa-park-hotel-6.jpg' }),
      
    ];
  }

}
