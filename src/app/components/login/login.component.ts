import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private _http:HttpClient, 
    ) { }

  ngOnInit(): void {
    if(this.authService.isAuthenticated()) {
      this.router.navigate(['/bookings']);
    }

    this.formGroup = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  goToRegister(): void {
    this.router.navigate(['register'])
  }

  login() {
     this._http.get<any>(`${environment.apiUrl}/users`)
     .subscribe({
      next: (result) => {
        const user = result.find((a:any)=>{
          return a.email === this.formGroup.value.email && a.password === this.formGroup.value.password
        });

       if(user) {
        this.authService.setUserInStorage(user);
        this.formGroup.reset();
        this.router.navigate(['bookings']);
      } else {
        alert('User Not Found');
        this.router.navigate(['login']);
      }
      },
      error: () => alert('Something was wrong') 
     })
  }
}
