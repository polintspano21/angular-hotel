import { Component, OnInit } from '@angular/core';
import { Gallery, GalleryItem, ImageItem } from 'ng-gallery';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {
  images:GalleryItem[];
  constructor() {
    this.images=[];
   }

  ngOnInit(): void {
    this.images = [
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-panorama-arte-spa-park-hotel.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-panorama-arte-spa-park-hotel.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-panorama-arte-spa-park-hotel-1-1.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-panorama-arte-spa-park-hotel-1-1.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/Dvoina-Staq-Delux-Panorama-Arte-Spa-Park-Hotel-Velingrad-10.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/Dvoina-Staq-Delux-Panorama-Arte-Spa-Park-Hotel-Velingrad-10.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/Ednospalen-Apartament-Arte-Spa-Park-Hotel-11.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/Ednospalen-Apartament-Arte-Spa-Park-Hotel-11.jpg' }),
      new ImageItem({ src: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-arte-spa-park-hotel-6.jpg', thumb: 'https://artehotel.bg/php_assets/uploads/2019/10/dvoina-staq-delux-arte-spa-park-hotel-6.jpg' }),
      
    ];
  }

}
