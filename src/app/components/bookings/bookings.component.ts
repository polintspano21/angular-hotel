import { HttpClient } from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { faL } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/auth/auth.service';
import { UserRole } from 'src/app/models/User.model';
import { environment } from 'src/environments/environment';

export interface Booking {
  id: number;
  userName: string;
  roomType: string;
  days: number;
  approved: boolean;
}

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit {
  displayedColumns: string[] = ['id', 'userName', 'roomType', 'days', 'approved', 'actions'];
  canApprove: boolean;
  dataSource = new MatTableDataSource<Booking>();
  formGroup!: FormGroup;
  select = "Единична";

  constructor(
    private _http: HttpClient,
    private fb: FormBuilder,
    public authService: AuthService
    ) {
      this.canApprove = authService.getUserFromStorage().role === UserRole.ADMINISTRATOR;
    }

  ngOnInit(): void {
    this.getBookings()
    this.formGroup = this.fb.group({
      endDate: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      days: ['', [Validators.required]],
    });
  }

  getBookings() {
    this._http.get<Booking[]>(`${environment.apiUrl}/bookings`)
    .subscribe({
     next: (result) =>{ this.dataSource.data = result; console.log(result)},
     error: () => alert('Something was wrong') 
    })
  }

  reserve() {
    this._http.post<Booking>(`${environment.apiUrl}/bookings`, {...this.formGroup.value, roomType: this.select, approved: false, userName: this.authService.getUserFromStorage().name})
     .subscribe({
      next: () => this.getBookings(),
      error: () => alert('Something was wrong') 
     })
  }

  approve(id: number, approveParam: boolean) {
    const booking = this.dataSource.data.find((booking) => booking.id === id)
    this._http.put<Booking>(`${environment.apiUrl}/bookings/${id}`, {...booking, approved: approveParam})
     .subscribe({
      next: () => this.getBookings(),
      error: () => alert('Something was wrong') 
     })
  }
}