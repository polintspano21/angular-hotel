export enum UserRole {
    NONE = "NONE",
    USER = "USER",
    ADMINISTRATOR = "ADMINISTRATOR"
}

export interface User {
    id: number,
    name: string,
    email: string,
    password: string,
    role: UserRole,
}