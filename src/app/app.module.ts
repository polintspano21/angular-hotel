import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './components/navigation/navigation.component';
import { MatTabsModule } from '@angular/material/tabs';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GalleryComponent } from './components/gallery/gallery.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { BookingsComponent } from './components/bookings/bookings.component';
import { UnitsComponent } from './components/units/units.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { GalleryModule } from 'ng-gallery';
import { RoomComponent } from './components/room/room.component';
import { MatIconModule } from '@angular/material/icon';
import { RoomSingleComponent } from './components/room-single/room-single.component';
import { ApartmentComponent } from './components/apartment/apartment.component';
import { ApartamentVipComponent } from './components/apartament-vip/apartament-vip.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    GalleryComponent,
    LoginComponent,
    RegisterComponent,
    BookingsComponent,
    UnitsComponent,
    HomeComponent,
    RoomComponent,
    RoomSingleComponent,
    ApartmentComponent,
    ApartamentVipComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    FontAwesomeModule,
    MatGridListModule,
    HttpClientModule,
    GalleryModule,
    MatIconModule,
    ReactiveFormsModule,
    MatTableModule,
    MatIconModule,
    MatInputModule,
    MatDatepickerModule,
    MatButtonModule,
    MatSelectModule,
  ],
  providers: [
    AuthGuardService, 
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
