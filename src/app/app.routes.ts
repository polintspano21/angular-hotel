import { Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { BookingsComponent } from './components/bookings/bookings.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { RoomComponent } from './components/room/room.component';
import { RoomSingleComponent } from './components/room-single/room-single.component';
import { ApartmentComponent } from './components/apartment/apartment.component';
import { ApartamentVipComponent } from './components/apartament-vip/apartament-vip.component';

export const ROUTES: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { 
        path: 'bookings',
        component: BookingsComponent,
        canActivate: [AuthGuard]
    },
    { path: 'room', component: RoomComponent },
    { path: 'room-single', component: RoomSingleComponent },
    { path: 'apartment', component: ApartmentComponent },
    { path: 'apartament-vip', component: ApartamentVipComponent },
    { path: 'register', component: RegisterComponent }
];